export class Computer {
    constructor(public id?:number,
                public ownerName?:string,
                public ip?:string) {}
}