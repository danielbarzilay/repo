import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Computer } from 'src/app/classes/computer';
import { User } from 'src/app/classes/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProjectserviceService {

  constructor(private client:HttpClient, private router:Router) { }
  
  public getUsers(): Observable<any> {
    return this.client.get<any>("../user/get_all");
  }

  public register(user:User): Observable<any> {
    return this.client.post<any>("../user/create", user);
  }

  public addComputer(computer:Computer): Observable<any> {
    return this.client.post<any>("../computer/create", computer);
  }

  public editComputer(computer:Computer): Observable<any> {
    return this.client.put<any>("../computer/update", computer);
  }

  public getComputers(): Observable<any> {
    return this.client.get<any>("../computer/get_all");
  }

  public removeComputer(id:number): Observable<any> {
    return this.client.delete<any>("../computer/remove/" + id);
  }

  public getPing(ip:string): Observable<any> {
    return this.client.get<any>("../computer/get_ping/" + ip);
  }

  public logout(): void {
    this.router.navigate(['/']);
  }
  
}