import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ProjectserviceService } from "src/app/services/projectservice.service";
import { Router } from '@angular/router';
import { User } from 'src/app/classes/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public flag:boolean = false;
  public output:string;
  public exception:string;
  public user:User = new User();
  private response:HttpErrorResponse;
  

  constructor(private service:ProjectserviceService, 
    private router:Router) { }

  ngOnInit() {
  }

  public registerFoo(): void {
    this.service.register(this.user).subscribe(content => {
      this.output = content; 
      this.flag = true;
    }, fail => {
      this.response = fail;
      this.exception = this.response.error.message;
    })
  }

  public navigateLogin(): void {
    this.router.navigate(['']);
  }

}