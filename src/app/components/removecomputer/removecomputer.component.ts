import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ProjectserviceService } from "src/app/services/projectservice.service";
import { Computer } from "src/app/classes/computer";
import { Router } from '@angular/router';

@Component({
  selector: 'app-removecomputer',
  templateUrl: './removecomputer.component.html',
  styleUrls: ['./removecomputer.component.css']
})
export class RemovecomputerComponent implements OnInit {
  public computer:Computer = new Computer();
  public flag:boolean = false;
  public exception:string;
  private response:HttpErrorResponse;
  private computers:Computer[] = [];

  constructor(private service:ProjectserviceService) { }

  ngOnInit() {
  }

  public removeComputerFoo(): void {
    this.service.getComputers().subscribe ( content => {
      this.computers = content;
        if (this.computers.length > 0) {
          for (let i = 0; i < this.computers.length; i++) {
            if (this.computers[i].ownerName === this.computer.ownerName && this.computers[i].ip === this.computer.ip) {
              this.service.removeComputer(this.computers[i].id).subscribe ( content => {
              this.flag = true;
              })
            break;
            }       
          }
          if(!this.flag) {
            this.exception = 'No computer with the specified credentials exist';
          }
        } else {
          this.exception = 'No computers have found';
        }
    })
  }

}