import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemovecomputerComponent } from './removecomputer.component';

describe('RemovecomputerComponent', () => {
  let component: RemovecomputerComponent;
  let fixture: ComponentFixture<RemovecomputerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemovecomputerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemovecomputerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
