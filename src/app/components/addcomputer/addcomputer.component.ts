import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ProjectserviceService } from "src/app/services/projectservice.service";
import { Computer } from "src/app/classes/computer";
import { Router } from '@angular/router';

@Component({
  selector: 'app-addcomputer',
  templateUrl: './addcomputer.component.html',
  styleUrls: ['./addcomputer.component.css']
})
export class AddcomputerComponent implements OnInit {
  public computer:Computer = new Computer();
  public flag:boolean = true;
  public failFlag:boolean = false;
  public exception:string;
  public output:string;
  private computers:Computer[] = [];
  private response:HttpErrorResponse;

  constructor(private service:ProjectserviceService ) { }

  ngOnInit() {
  }

  public addComputerFoo(): void {
    this.service.getComputers().subscribe ( content => {
      this.computers = content;
      if (this.computers.length > 0) {
        for (let i = 0; i < this.computers.length; i++) {
          if (this.computers[i].ip === this.computer.ip) {
            this.output = 'A computer with the specified ip already exist';
            this.flag = false;
            break;
          }
        }
        if (this.flag) {
          this.service.addComputer(this.computer).subscribe( content => {
            this.output = 'Your computer had been created successfully';
          }, fail => {
            this.response = fail;
            this.exception = this.response.error.message;
            this.failFlag = true;
          })
        } 
        this.flag = true;
      } else {
        this.service.addComputer(this.computer).subscribe( content => {
          this.output = 'Your computer had been created successfully';
        }, fail => {
          this.response = fail;
          this.exception = this.response.error.message;
          this.failFlag = true;
        })
      }
    })
  }

}