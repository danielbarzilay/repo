import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcomputerComponent } from './addcomputer.component';

describe('AddcomputerComponent', () => {
  let component: AddcomputerComponent;
  let fixture: ComponentFixture<AddcomputerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcomputerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcomputerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
