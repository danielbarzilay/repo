import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ProjectserviceService } from "src/app/services/projectservice.service";
import { Router } from '@angular/router';
import { Computer } from "src/app/classes/computer";

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {
  public flag:boolean = false;
  public computers:Computer[] = [];
  public exception:string;
  public ping:string[] = [];
  public pos:number;
  public pingFlag:boolean = false;
  public failPingFlag = false;
  private response:HttpErrorResponse;
  

  constructor(private service:ProjectserviceService) { 
  }

  ngOnInit() {
    this.getComputersFoo();
  }

  public getComputersFoo(): void {
    this.service.getComputers().subscribe( content => {
      this.computers = content; 
      if (this.computers.length > 0) {
        this.flag = true;
      } else {
        this.exception = 'No computers available';
      }
   }, fail => {
     this.response = fail;
     this.exception = this.response.error.message;
     this.flag = false;
   })
  }

  public getPingFoo(ip:string, i:number): void {
    this.service.getPing(ip).subscribe( content => {
      if ((this.pingFlag) && (this.computers.length > 1)) {
       this.ping[this.pos] = undefined;
       this.ping[i] = content;
       this.pos = i;
      } else if (!this.pingFlag) {
      this.ping[i] = content;
      this.pos = i;
      this.pingFlag = true;
      } else {
        this.ping[i] = content;
      }
    }, fail => {
      this.response = fail;
      this.exception = this.response.error.message;
      this.failPingFlag = true;
    })
  }

}