import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ProjectserviceService } from "src/app/services/projectservice.service";
import { Router } from '@angular/router';
import { User } from 'src/app/classes/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user:User = new User();
  public flag:boolean = false;
  public exception:string;
  private users:User[] = [];
  private response:HttpErrorResponse;

  constructor(private service:ProjectserviceService, 
    private router:Router) { }

  ngOnInit() {
  }

  public login(): void {
    this.service.getUsers().subscribe( content => {
      this.users = content
      if (this.users.length > 0) {
        for (let i = 0; i < this.users.length; i++) {
          if (this.users[i].username === this.user.username && this.users[i].password === this.user.password) {
            this.router.navigate(['/main_page']);
            this.flag = true;
            break;
          }
        }
        if (!this.flag) {
          this.exception = 'No account with the specific credentials had been found';
        }
      } else {
          this.exception = 'No accounts exist in the system';
      }
    })
  }

  public navigateRegister() : void {
    this.router.navigate(['/register']);
  }

}