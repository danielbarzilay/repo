import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditcomputerComponent } from './editcomputer.component';

describe('EditcomputerComponent', () => {
  let component: EditcomputerComponent;
  let fixture: ComponentFixture<EditcomputerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditcomputerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditcomputerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
