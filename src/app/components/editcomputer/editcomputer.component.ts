import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ProjectserviceService } from "src/app/services/projectservice.service";
import { Computer } from "src/app/classes/computer";
import { Router } from '@angular/router';

@Component({
  selector: 'app-editcomputer',
  templateUrl: './editcomputer.component.html',
  styleUrls: ['./editcomputer.component.css']
})
export class EditcomputerComponent implements OnInit {
  public preComputer:Computer = new Computer();
  public computer:Computer = new Computer();
  public flag:boolean = false;
  public exception:string;
  private computers:Computer[] = [];
  private response:HttpErrorResponse;

  constructor(private service:ProjectserviceService) { }

  ngOnInit() {
  }

  public editComputerFoo(): void {
    this.service.getComputers().subscribe ( content => {
      this.computers = content;
      if (this.computers.length > 0) {
        for (let i = 0; i < this.computers.length; i++) {
          if ((this.computers[i].ownerName === this.preComputer.ownerName) && (this.computers[i].ip === this.preComputer.ip)) {
            this.computer.id = this.computers[i].id;
            this.service.editComputer(this.computer).subscribe (content => {
            this.flag = true;
            })
            break;
          }       
        }
        if(!this.flag) {
          this.exception = 'No computer owner with the specified credentials exist';
        }
      } else {
        this.exception = 'No computers have found';
      }
    })
  }

}