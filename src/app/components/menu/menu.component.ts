import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectserviceService } from 'src/app/services/projectservice.service';
import { Url } from 'src/app/classes/url';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  public pos:number;
  public urls:Url[] = [{path: '/main_page', name: 'Main Page'},
                       {path: '/add_computer', name: 'Add Computer'},
                       {path: '/edit_computer', name: 'Edit Computer'},
                       {path: '/remove_computer', name: 'Remove Computer'},
                       {path: '', name: 'Logout'}];

  constructor(private route:Router,
              private service:ProjectserviceService) { }

  ngOnInit() { 
  }

  public navigateFoo(index:number): void {
    this.route.navigate([this.urls[index].path]);
  }

}