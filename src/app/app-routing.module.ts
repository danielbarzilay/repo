import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddcomputerComponent } from './components/addcomputer/addcomputer.component';
import { EditcomputerComponent } from './components/editcomputer/editcomputer.component';
import { LoginComponent } from './components/login/login.component';
import { MainpageComponent } from './components/mainpage/mainpage.component';
import { RegisterComponent } from './components/register/register.component';
import { RemovecomputerComponent } from './components/removecomputer/removecomputer.component';

const routes: Routes = [
  {path:'', component:LoginComponent},
  {path:'register', component:RegisterComponent},
  {path:'main_page', component:MainpageComponent},
  {path:'add_computer', component:AddcomputerComponent},
  {path:'edit_computer', component:EditcomputerComponent},
  {path:'remove_computer', component:RemovecomputerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
