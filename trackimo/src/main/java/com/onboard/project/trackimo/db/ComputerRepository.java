package com.onboard.project.trackimo.db;

import org.springframework.data.jpa.repository.JpaRepository;
import com.onboard.project.trackimo.beans.Computer;

public interface ComputerRepository extends JpaRepository<Computer, Long>{
}