package com.onboard.project.trackimo.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.onboard.project.trackimo.beans.Computer;
import com.onboard.project.trackimo.db.ComputerDBDAO;

@RestController
@RequestMapping(path = "computer")
public class ComputerService {
	@Autowired
	private ComputerDBDAO compData;
	
	@PostMapping(path = "/create")
	public ResponseEntity<String> createComputer(@RequestBody Computer comp) {
		compData.create(comp);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	@PutMapping(path = "/update")
	public ResponseEntity<String> updateComputer(@RequestBody Computer comp) {
		return new ResponseEntity<String>(compData.update(comp), HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/remove/{id}")
	public ResponseEntity<String> removeComputer(@PathVariable("id") long idVar) {
		compData.remove(idVar);
		return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping(path = "/get_all")
	public ResponseEntity<List<Computer>> getAllComputers() {
		return new ResponseEntity<List<Computer>>(compData.getAll(), HttpStatus.OK);
	}
	
	@GetMapping(path = "/get_ping/{ip}")
	public ResponseEntity<String> getIp(@PathVariable("ip") String ip) {
		List<String> commands = new ArrayList<String>();
		commands.add("ping");
		commands.add(ip);
		ProcessBuilder build = new ProcessBuilder(commands);
		String row;
		int x = 0;
		List<String> packet = new ArrayList<String>();
		List<Error> exceptions= new ArrayList<Error>();
		try {
			Process process = build.start();
			BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
			BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			for (int i = 0; (row = input.readLine()) != null; i++) {
				packet.add(row);
			}
			if (packet.size() == 11) {
				packet = packet.subList(7, 11);
			}
			row = null;
			input.close();
				
			for (int i = 0; (row = error.readLine()) != null; i++) {
				exceptions.add(new Error(row));
			}
			if (exceptions.size() > 0 ) {
				System.out.println(exceptions);
			}
			row = null;
			error.close();
					
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<String>(new Gson().toJson(String.join("", packet)), HttpStatus.OK);
	}
	
}