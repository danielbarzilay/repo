package com.onboard.project.trackimo.db;

import org.springframework.data.jpa.repository.JpaRepository;
import com.onboard.project.trackimo.beans.User;

public interface UserRepository  extends JpaRepository<User, Long>{
}