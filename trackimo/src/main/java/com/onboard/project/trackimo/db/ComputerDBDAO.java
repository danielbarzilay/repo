package com.onboard.project.trackimo.db;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.google.gson.Gson;
import com.onboard.project.trackimo.beans.Computer;

@Repository
public class ComputerDBDAO {
	@Autowired
	private ComputerRepository repo;

	public void create(Computer comp) {
		repo.save(comp);
	}
	
	public String update(Computer comp) {
		repo.save(comp);
		return new Gson().toJson("Computer had been updated successfully");
	}
	
	public void remove(long id) {
		repo.deleteById(id);
	}
	
	public List<Computer> getAll() {
		List<Computer> computers = repo.findAll();
		return computers;
	}
	
}