package com.onboard.project.trackimo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import com.onboard.project.trackimo.beans.Computer;
import com.onboard.project.trackimo.beans.User;
import com.onboard.project.trackimo.db.ComputerDBDAO;
import com.onboard.project.trackimo.db.ComputerRepository;
import com.onboard.project.trackimo.db.UserDBDAO;
import com.onboard.project.trackimo.db.UserRepository;
import com.onboard.project.trackimo.web.ComputerService;
import com.onboard.project.trackimo.web.UserService;

@SpringBootApplication
class TrackimoApplication {

	public static void main(String[] args) {
		  SpringApplication.run(TrackimoApplication.class, args);		
	}

}