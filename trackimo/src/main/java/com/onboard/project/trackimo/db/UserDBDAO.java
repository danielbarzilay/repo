package com.onboard.project.trackimo.db;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.google.gson.Gson;
import com.onboard.project.trackimo.beans.User;

@Repository
public class UserDBDAO {
	@Autowired
	private UserRepository repo;
	
	public String create(User user) {
		List<User> users = repo.findAll();
		if (users.size() >= 1) {
			for (int i = 0; i < users.size(); i++) {
				if (users.get(i).getUsername().equalsIgnoreCase(user.getUsername())) {
					return new Gson().toJson("A user with the same username already exist, please choose a different username and try again");
				}
			}
			repo.save(user);
			return new Gson().toJson("User had been created successfully, you can now login");
		} 
		repo.save(user);
		return new Gson().toJson("User had been created successfully, you can now login");
	}
	
	public List<User> getAll() {
		List<User> users = repo.findAll();
		return users;
	}
	
}