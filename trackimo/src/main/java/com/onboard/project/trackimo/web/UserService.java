package com.onboard.project.trackimo.web;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.onboard.project.trackimo.beans.User;
import com.onboard.project.trackimo.db.UserDBDAO;

@RestController
@RequestMapping(path = "user")
public class UserService {
	@Autowired
	private UserDBDAO userData;
	
	@PostMapping(path = "/create")
	public ResponseEntity<String> createUser(@RequestBody User user) {
		return new ResponseEntity<String>(userData.create(user), HttpStatus.CREATED);
	}
	
	@GetMapping(path = "/get_all")
	public ResponseEntity<List<User>> getAll() {
		return new ResponseEntity<List<User>>(userData.getAll(), HttpStatus.OK);
	}

}